package com.krystianrozbiecki.shop.online.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.krystianrozbiecki.shop.online.domain.Product;
import com.krystianrozbiecki.shop.online.domain.repository.ProductRepository;
import com.krystianrozbiecki.shop.online.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;

	@Override
	public List<Product> getAllProducts() {

		return productRepository.getAllProducts();
	}

	@Override
	public List<Product> getProductsByCategory(String category) {
		return productRepository.getProductsByCategory(category);
	}

	@Override
	public Set<Product> getProductsByFilter(Map<String, List<String>> filterParams) {
		return productRepository.getProductsByFilter(filterParams);
	}

	@Override
	public Product getProductById(String productId) {
		return productRepository.getProductById(productId);
	}

	@Override
	public List<Product> getProductsByManufacturer(String manufacturer) {
		return productRepository.getProductsByCategory(manufacturer);
	}

	@Override
	public List<Product> getProductsByPriceRange(BigDecimal low, BigDecimal high) {
		return productRepository.getProductsByPriceRange(low, high);
	}

	@Override
	public void addProduct(Product product) {
		productRepository.addProduct(product);
	}

}
