package com.krystianrozbiecki.shop.online.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.krystianrozbiecki.shop.online.domain.Customer;
import com.krystianrozbiecki.shop.online.domain.repository.CustomerRepository;
import com.krystianrozbiecki.shop.online.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	CustomerRepository customerRepository;
	
	@Override
	public List<Customer> getAllCustomers() {
		return customerRepository.getAllCustomers();
	}

}
