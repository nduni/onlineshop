package com.krystianrozbiecki.shop.online.service;

public interface OrderService {
	void processOrder(String productId, int count);
}
