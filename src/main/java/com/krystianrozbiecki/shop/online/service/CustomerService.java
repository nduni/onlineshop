package com.krystianrozbiecki.shop.online.service;

import java.util.List;

import com.krystianrozbiecki.shop.online.domain.Customer;

public interface CustomerService {
	List<Customer> getAllCustomers();
}
