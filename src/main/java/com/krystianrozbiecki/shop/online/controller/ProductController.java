package com.krystianrozbiecki.shop.online.controller;

import java.io.File;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.MatrixVariable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.krystianrozbiecki.shop.online.domain.Product;
import com.krystianrozbiecki.shop.online.exception.ProductNotFoundException;
import com.krystianrozbiecki.shop.online.service.ProductService;

@Controller
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductService productService;

	@InitBinder
	public void initialiseBinder (WebDataBinder binder) {
		binder.setDisallowedFields("unitsInOrder","discontinued");
		binder.setAllowedFields("productId","name","unitPrice","description","manufacturer","category","unitsInStock","productImage","condition","language");
	}
	@RequestMapping()
	public String list(Model model) {

		model.addAttribute("products", productService.getAllProducts());
		return "products";
	}

	@RequestMapping("/all")
	public String allProducts(Model model) {

		model.addAttribute("products", productService.getAllProducts());
		return "products";
	}

	@RequestMapping("/{category}")
	public String getProductsByCategory(Model model, @PathVariable("category") String productCategory) {
		model.addAttribute("products", productService.getProductsByCategory(productCategory));
		return "products";
	}

	@RequestMapping("/filter/{ByCriteria}")
	public String getProductsByFilter(@MatrixVariable(pathVar = "ByCriteria") Map<String, List<String>> filterParams,
			Model model) {
		model.addAttribute("products", productService.getProductsByFilter(filterParams));
		return "products";
	}

	@RequestMapping("/product")
	public String getProductById(@RequestParam("id") String productId, Model model) {
		model.addAttribute("product", productService.getProductById(productId));
		return "product";
	}

	@RequestMapping("/{category}/{ByPrice}")
	public String getProductsByCategoryPricaAndManufacturer(@PathVariable("category") String productCategory,
			@MatrixVariable(pathVar = "ByPrice") Map<String, List<String>> priceParams,
			@RequestParam("manufacturer") String manufacturer, Model model) {

		Set<Product> productsByCategory = new HashSet<Product>(productService.getProductsByCategory(productCategory));
		Set<Product> productsByManufacturer = new HashSet<Product>(productService.getProductsByManufacturer(manufacturer));

		BigDecimal low;
		BigDecimal high;

		low = new BigDecimal(priceParams.get("low").get(0));
		System.out.println(low);
		high = new BigDecimal(priceParams.get("high").get(0));
		System.out.println(high);

		Set<Product> productsByPrice = new HashSet<Product>(productService.getProductsByPriceRange(low, high));

		Set<Product> filteredResult = new HashSet<Product>(productsByCategory);

		filteredResult.retainAll(productsByManufacturer);
		filteredResult.retainAll(productsByPrice);

		model.addAttribute("products", filteredResult);

		return "products";
	}
	
	@RequestMapping(value="/add", method= RequestMethod.GET)
	public String getAffNewProductForm(Model model) {
		Product newProduct = new Product();
		model.addAttribute("newProduct", newProduct);
		return "addProduct";
	}
	@RequestMapping(value="/add",method= RequestMethod.POST)
	public String processAddNewProductForm(@ModelAttribute("newProduct") Product productToBeAdded, BindingResult result, HttpServletRequest request) {
		String[] suppressedFields= result.getSuppressedFields();
		if(suppressedFields.length>0) {
			throw new RuntimeException("Próba wiązania niedozwolonych pól: "+StringUtils.arrayToCommaDelimitedString(suppressedFields));
		}
		MultipartFile productImage= productToBeAdded.getProductImage();
		String rootDirectory = request.getSession().getServletContext().getRealPath("/");
		System.out.println(rootDirectory);
		if (productImage!=null && !productImage.isEmpty()) {
			try {
				productImage.transferTo(new File(rootDirectory+"resources\\static\\images\\"+productToBeAdded.getProductId()+".png"));
			} catch(Exception e) {
				throw new RuntimeException("Niepowodzenie podczas próby zapisu obrazka produktu",e);
			}
		}
		productService.addProduct(productToBeAdded);
		
		return "redirect:/products";
	}
	@ExceptionHandler(ProductNotFoundException.class)
	public ModelAndView handleError(HttpServletRequest req, ProductNotFoundException exception) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("invalidProductId",exception.getProductId());
		mav.addObject("exception", exception);
		mav.addObject("url", req.getRequestURI()+"?"+req.getQueryString());
		mav.setViewName("productNotFound");
		return mav;
	}
}
