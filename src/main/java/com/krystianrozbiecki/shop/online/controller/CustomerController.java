package com.krystianrozbiecki.shop.online.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.krystianrozbiecki.shop.online.service.CustomerService;

@Controller
public class CustomerController {
	@Autowired
	private CustomerService customerService;
	
	@RequestMapping("customers")
	public String customersList(Model model) {
		model.addAttribute("customers", customerService.getAllCustomers());
		
		return "customers";
	}
}
