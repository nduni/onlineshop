package com.krystianrozbiecki.shop.online.domain.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.krystianrozbiecki.shop.online.domain.Product;
import com.krystianrozbiecki.shop.online.domain.repository.ProductRepository;
import com.krystianrozbiecki.shop.online.exception.ProductNotFoundException;

@Repository
public class InMemoryProductRepository implements ProductRepository {

	private List<Product> listOfProducts = new ArrayList<Product>();

	public InMemoryProductRepository() {

		Product iphone = new Product("P1234", "ïPhone 5s", new BigDecimal(500));
		iphone.setDescription(
				"Apple iPhone 5s, smartfon z 4-calowym wyświetlaczem o rozdzeilczości 640x1136 oraz 8-megapikselowym aparatem");
		iphone.setCategory("Smart Phone");
		iphone.setManufacturer("Apple");
		iphone.setUnitsInStock(1000);

		Product laptop_dell = new Product("P1235", "Dell Inspiron", new BigDecimal(700));
		laptop_dell.setDescription("Dell Inspiron, 14-calowy laptop (czarny) z procesorem Intel Core 3. generacji");
		laptop_dell.setCategory("Laptop");
		laptop_dell.setManufacturer("Dell");
		laptop_dell.setUnitsInStock(1000);

		Product tablet_Nexus = new Product("P1236", "Nexus 7", new BigDecimal(300));
		tablet_Nexus.setDescription(
				"Google Nexus 7 jest najlżejszym 7-calowym tabletem z 4-rdzeniowym procesorem Qualcomm Snapdragon S4 Pro");
		tablet_Nexus.setCategory("Tablet");
		tablet_Nexus.setManufacturer("Google");
		tablet_Nexus.setUnitsInStock(1000);

		listOfProducts.add(iphone);
		listOfProducts.add(laptop_dell);
		listOfProducts.add(tablet_Nexus);
	}

	@Override
	public List<Product> getAllProducts() {
		return listOfProducts;
	}

	@Override
	public Product getProductById(String productId) {
		Product productById = null;
		for (Product product : listOfProducts) {
			if (product != null && product.getProductId() != null && product.getProductId().equals(productId)) {
				productById = product;
				break;
			}
		}
		if (productById == null) {
			throw new ProductNotFoundException(productId);
		}
		return productById;
	}

	@Override
	public List<Product> getProductsByCategory(String category) {
		List<Product> productsByCategory = new ArrayList<Product>();
		for (Product p : listOfProducts) {
			if (category.equalsIgnoreCase(p.getCategory())) {
				productsByCategory.add(p);
			}
		}
		return productsByCategory;
	}

	@Override
	public Set<Product> getProductsByFilter(Map<String, List<String>> filterParams) {
		Set<Product> productsByBrand = new HashSet<Product>();
		Set<Product> productsByCategory = new HashSet<Product>();
		Set<String> criterias = filterParams.keySet();
		if (criterias.contains("brand")) {
			for (String brandName : filterParams.get("brand")) {
				for (Product p : listOfProducts) {
					if(brandName.equalsIgnoreCase(p.getManufacturer())) {
						productsByBrand.add(p);
					}
				}
			}	
		}
		if (criterias.contains("category")) {
			for(String category: filterParams.get("category")) {
				productsByCategory.addAll(this.getProductsByCategory(category));
			}
		}
		productsByCategory.retainAll(productsByBrand);
		return productsByCategory;
	}

	@Override
	public List<Product> getProductsByManufacturer(String manufacturer) {
		List<Product> productsByManufacturer = new ArrayList<Product>();
		for (Product p : listOfProducts) {
			if (manufacturer.equalsIgnoreCase(p.getManufacturer())) {
				productsByManufacturer.add(p);
			}
		}
		return productsByManufacturer;
	}

	@Override
	public List<Product> getProductsByPriceRange(BigDecimal low, BigDecimal high) {
		List<Product> productsByPriceRange = new ArrayList<Product>();
		for (Product p: listOfProducts) {
			if (p.getUnitPrice().compareTo(low)>=0 && p.getUnitPrice().compareTo(high) <= 0 ) {
				productsByPriceRange.add(p);
			}
		}
		return productsByPriceRange;
	}

	@Override
	public void addProduct(Product product) {
		listOfProducts.add(product);
		
	}
	
}
