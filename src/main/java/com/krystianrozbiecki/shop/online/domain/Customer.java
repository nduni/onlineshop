package com.krystianrozbiecki.shop.online.domain;

public class Customer {
	private String customertId;
	private String name;
	private String address;
	private int noOfOrdersName;
	
	public Customer() {
		super();
	}

	public Customer(String customertId, String name) {
		super();
		this.customertId = customertId;
		this.name = name;
	}

	public String getCustomertId() {
		return customertId;
	}

	public void setCustomertId(String customertId) {
		this.customertId = customertId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getNoOfOrdersName() {
		return noOfOrdersName;
	}

	public void setNoOfOrdersName(int noOfOrdersName) {
		this.noOfOrdersName = noOfOrdersName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((customertId == null) ? 0 : customertId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (customertId == null) {
			if (other.customertId != null)
				return false;
		} else if (!customertId.equals(other.customertId))
			return false;
		return true;
	}
	
	
}
