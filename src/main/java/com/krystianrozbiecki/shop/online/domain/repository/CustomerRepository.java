package com.krystianrozbiecki.shop.online.domain.repository;

import java.util.List;

import com.krystianrozbiecki.shop.online.domain.Customer;

public interface CustomerRepository {
	List<Customer> getAllCustomers();
}
