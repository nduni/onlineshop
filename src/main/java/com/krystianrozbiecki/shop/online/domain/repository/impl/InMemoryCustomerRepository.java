package com.krystianrozbiecki.shop.online.domain.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.krystianrozbiecki.shop.online.domain.Customer;
import com.krystianrozbiecki.shop.online.domain.repository.CustomerRepository;

@Repository
public class InMemoryCustomerRepository implements CustomerRepository {

	List<Customer> listOfCustomers = new ArrayList<>();
	
	public InMemoryCustomerRepository() {

		Customer adam = new Customer("C1234", "Adam");
		adam.setAddress("Wrocław, ul. Piłsudskiego 14");
		adam.setNoOfOrdersName(2);
		
		Customer ewa = new Customer("C1235", "Ewa");
		ewa.setAddress("Wrocław, ul. Szewska 67");
		ewa.setNoOfOrdersName(0);
		
		Customer janusz = new Customer("C1236", "Janusz");
		janusz.setAddress("Wrocław, ul. Legnicka 60");
		janusz.setNoOfOrdersName(10);
		
		listOfCustomers.add(adam);
		listOfCustomers.add(ewa);
		listOfCustomers.add(janusz);
	}
	@Override
	public List<Customer> getAllCustomers() {

		return listOfCustomers;
	}

}
