package com.krystianrozbiecki.shop.online.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

	@Configuration
	
	@ComponentScan(basePackages="com.krystianrozbiecki.shop.online")

	public class Views {
		
	 
	    @Bean
	    public ViewResolver contentNegotiatingViewResolver(ContentNegotiationManager manager) {
	    	InternalResourceViewResolver resolver = new InternalResourceViewResolver();
	        resolver.setPrefix("/WEB-INF/view/");
	        resolver.setSuffix(".jsp");
	        return resolver;
	    }
	 
	 

	   
	
}
