package com.krystianrozbiecki.shop.online.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

		@Override
		protected void configure(HttpSecurity http) throws Exception{
			  http.authorizeRequests()
	            .antMatchers("/products/add").hasRole("ADMIN")
	            .antMatchers("/**").permitAll()
	            	.and()
	            		.formLogin()
	            		.loginPage("/login")
	            		.loginProcessingUrl("/authenticateTheUser")
	            		.permitAll()
	            		.defaultSuccessUrl("/products/add")
	            		.failureUrl("/loginfailed")
	            	.and()
	            		.logout()
	            		.logoutUrl("/logout")
	            	.and()
	            		.csrf().disable()
	            ;
        }
		@Autowired
		public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
			auth
				.inMemoryAuthentication()
					.withUser("admin").password("{noop}Admin123").roles("ADMIN");
		}
}
