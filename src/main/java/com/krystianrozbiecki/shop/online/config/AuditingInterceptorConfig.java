package com.krystianrozbiecki.shop.online.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.krystianrozbiecki.shop.online.interceptor.AuditingInterceptor;

public class AuditingInterceptorConfig implements WebMvcConfigurer {

	@Autowired
	private AuditingInterceptor auditingInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(auditingInterceptor);
	}
}
